﻿using System.Net.Http.Headers;
using System.Threading.Tasks;
using Grpc.Net.Client;
using MessengerWebApp.DataAccess.Providers;
using MessengerWebApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace MessengerWebApp.Controllers
{
    [Route("api/chat")]
    [ApiController]
    [Authorize]
    public class ChatController : ControllerBase
    {
        private GrpcChannel channel = GrpcChannel.ForAddress("https://localhost:5001");
        private readonly PrivateChatProvider _chatProvider;
        private readonly UserProvider _userProvider;
        private readonly UserAuthenticationService _authenticationService;

        public ChatController(UserProvider userProvider, PrivateChatProvider chatProvider, UserAuthenticationService authenticationService)
        {
            _userProvider = userProvider;
            _chatProvider = chatProvider;
            _authenticationService = authenticationService;
        }

        [HttpPost]
        public async Task<IActionResult> Get(string message)
        {
            var user = await _authenticationService.GetUserByHeaders(Request.Headers[HeaderNames.Authorization]
                .ToArray());
            var client = new Greeter.GreeterClient(channel);
            var reply = await client.SayHelloAsync(new HelloRequest
            {
                Name = user.Username,
                Message = message
            });

            return Ok(reply.Message);
        }
    }
}