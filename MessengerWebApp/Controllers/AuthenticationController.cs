using System;
using System.Threading.Tasks;
using MessengerWebApp.Contracts.Dtos;
using MessengerWebApp.Contracts.ViewModels;
using MessengerWebApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MessengerWebApp.Controllers
{
    [Route("api/auth/")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserAuthenticationService _authenticationService;

        public AuthenticationController(UserAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost("signin")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> SignIn(LoginUserViewModel loginUserViewModel)
        {
            try
            {
                var token = await _authenticationService
                    .Authenticate(loginUserViewModel.Email, loginUserViewModel.Password);

                return token != null ? Ok(token) : Unauthorized();
            }
            catch (ArgumentException)
            {
                return Unauthorized();
            }
        }

        [HttpPost("signup")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(string))]
        public async Task<IActionResult> SignUp(RegisterUserViewModel registerUser)
        {
            string token;
            try
            {
                token = await _authenticationService.Register(new UserRegistrationDto
                {
                    UserName = registerUser.UserName,
                    Email = registerUser.Email,
                    Password = registerUser.Password
                });
            }
            catch (ArgumentException e)
            {
                return Unauthorized(e.Message);
            }

            return Ok(token);
        }
    }
}