﻿using System.Threading.Tasks;
using MessengerWebApp.Contracts.Parameters;
using MessengerWebApp.Contracts.Responses;
using MessengerWebApp.DataAccess.Providers;
using MessengerWebApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace MessengerWebApp.Controllers
{
    [Route("api/user/")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly UserAuthenticationService _authenticationService;
        private readonly UserProvider _userProvider;

        public UserController(UserProvider userProvider, UserAuthenticationService authenticationService)
        {
            _userProvider = userProvider;
            _authenticationService = authenticationService;
        }

        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            var user = await _authenticationService.GetUserByHeaders(Request.Headers[HeaderNames.Authorization]
                .ToArray());

            return Ok(new UserResponse
            {
                Email = user.Email,
                Username = user.Username
            });
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] PutUserParameter parameter)
        {
            var user = await _authenticationService.GetUserByHeaders(Request.Headers[HeaderNames.Authorization]
                .ToArray());
            user.Username = parameter.Username;
            await _userProvider.Edit(user);
            return NoContent();
        }
    }
}