﻿using MessengerWebApp.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace MessengerWebApp.DataAccess
{
    public sealed class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<PrivateChat> PrivateChats { get; set; }
        public DbSet<ChatMessage> ChatMessages { get; set; }
    }
}