﻿using System;

namespace MessengerWebApp.DataAccess.Models
{
    public class PrivateChat : Entity
    {
        public Guid FirstUserId { get; set; }
        public Guid SecondUserId { get; set; }
    }
}