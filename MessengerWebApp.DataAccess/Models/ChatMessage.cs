﻿using System;

namespace MessengerWebApp.DataAccess.Models
{
    public class ChatMessage : Entity
    {
        public string Message { get; set; }
        public Guid ChatId { get; set; }
        public Guid UserId { get; set; }
    }
}