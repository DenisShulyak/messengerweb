﻿using System;
using MessengerWebApp.DataAccess.Models;
using MessengerWebApp.DataAccess.Providers.Abstract;

namespace MessengerWebApp.DataAccess.Providers
{
    public class PrivateChatProvider : EntityProvider<ApplicationContext, PrivateChat, Guid>
    {
        private readonly ApplicationContext _context;

        public PrivateChatProvider(ApplicationContext context) : base(context)
        {
            _context = context;
        }
    }
}