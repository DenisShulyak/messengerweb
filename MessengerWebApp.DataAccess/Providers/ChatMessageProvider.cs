﻿using System;
using MessengerWebApp.DataAccess.Models;
using MessengerWebApp.DataAccess.Providers.Abstract;

namespace MessengerWebApp.DataAccess.Providers
{
    public class ChatMessageProvider : EntityProvider<ApplicationContext, ChatMessage, Guid>
    {
        private readonly ApplicationContext _context;

        public ChatMessageProvider(ApplicationContext context) : base(context)
        {
            _context = context;
        }
    }
}