﻿using System;
using System.Threading.Tasks;
using MessengerWebApp.DataAccess.Models;
using MessengerWebApp.DataAccess.Providers.Abstract;

namespace MessengerWebApp.DataAccess.Providers
{
    public class UserProvider : EntityProvider<ApplicationContext, User, Guid>
    {
        private readonly ApplicationContext _context;

        public UserProvider(ApplicationContext context) : base(context)
        {
            _context = context;
        }

        public async Task<User> GetByEmail(string email)
        {
            var user = await FirstOrDefault(x =>
                x.Email.ToLower().Equals(email.ToLower()));

            return user ?? throw new ArgumentException("User is not found");
        }
    }
}