﻿namespace MessengerWebApp.Contracts.Parameters
{
    public class PutUserParameter
    {
        public string Username { get; set; }
    }
}