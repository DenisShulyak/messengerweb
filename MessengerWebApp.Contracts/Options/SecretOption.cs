﻿namespace MessengerWebApp.Contracts.Options
{
    public class SecretOption
    {
        public string JwtSecret { get; set; }
    }
}